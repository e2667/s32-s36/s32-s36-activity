const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth")

//Route for create a course
router.post("/", auth.verify, (req,res) => {
const userRole = auth.decode(req.headers.authorization);
	console.log(userRole.isAdmin);

	if(userRole.isAdmin){
		courseController.addCourse(userRole.isAdmin, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
});

//Route for retrieving all the courses
router.get("/all",auth.verify, (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
});

router.get("/", (req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});

//Route for retrieving specific course
//URL: localhost:4000/courses/(id)
router.get("/:courseId", (req,res) => {
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});

//Route for updating a course
router.put("/:courseId", auth.verify, (req,res) => {
	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse: req.body
	}
	courseController.updateCourse(data).then(resultFromController => res.send(resultFromController));
});

//Activity S35
router.put('/:courseId/archive', auth.verify, (req, res) => {
        const data = {
                courseId: req.params.courseId,
                isAdmin: auth.decode(req.headers.authorization).isAdmin
        }

        courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;